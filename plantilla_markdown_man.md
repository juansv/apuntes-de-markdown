% COMANDO(1) comando 1.0.3
% NombreAutor Apellido
% Mayo 2022

# NAME
comando - comando es un comando que hace cosas de comandos.

# SYNOPSIS
**comando** [*OPCION*]\
**comando** *PATRON*

# DESCRIPTION
**comando** es un comando de la línea de comandos que hace cosas de comandos. Por ejemplo, saca un montón de texto por la consola, de tal forma que pareces un hacker del copón. Ejecutar **comando** te convierte en informático sin pasar por la universidad.
Por defecto, si al escribir la descripción pasas a otra línea con la tecla *intro*, la conversión al man insertará una línea en blanco de separación. Si no quieres que meta esa linea en blanco, debes cerrar la línea con un backslash (la barra para atrás: \).\
Esta línea no tiene separación.

## Caso particular
Cuando uses **comando**, mete alguna *pipe*, que parece más pro todavía.
Se pueden añadir subapartados en la descripción, pero sin pasarse: una entrada del man es una guía rápida de uso, no un manual de usuario a todo detalle.

# OPTIONS
**-h**, **--help**
: Muestra un mensaje de ayuda.

**-v**, **--verbose**
: Saca un chorrón de datos por pantalla.

**-e**, **--entrada DATO**
: Introduce un DATO de entrada a comando, de tal forma que comando hace más cosas molonas.

# EXAMPLES
**comando**
: Muestra un mensaje con instrucciones de uso, la versión y hace exit.

**comando -e DATO**
: Devuelve el dato multiplicado por pi, luego por e, le suma 3, le resta 5 y calcula el mínimo común múltiplo. ¿Y para qué? Pues para que salgan muchos datos por pantalla!!!

**comando -e | comando -v**
: Hace lo anterior, pero sacando muchos más datos por pantalla, para que mole más.

# EXIT VALUES
**0**
: Si todo sale bien, devuelve 0

**1**
: El DATO no es correcto

# BUGS
Espero que no haya...

# AUTHOR
Al escribir en la parte de arriba, donde los "%", el nombre del autor, no hace falta cumplimentar esta sección. Se indica aquí sólo como recordatorio de que, la convención de las entradas del man, el AUTHOR es una se las secciones obligadas.

# COPYRIGHT

Copyright 2020 Juan Silva. Licencia GPLv3+: GNU GPL version 3 o posterior <https://gnu.org/licences/gpl.html>. Esto es software libre: puedes cambiarlo y distribuirlo libremente. No hay garantía, en la medida que lo permita la ley.

