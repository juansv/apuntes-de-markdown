# Índice

[TOC]

# El mejor manual de markdown

El mejor manual de markdown en linea es: <https://www.w3schools.io/file/markdown-introduction/>


# Estructura de títulos

# Título de nivel 1

## Título de nivel 2

### Título de nivel 3

Texto sencillo, sin ningún tipo de separación de líneas ni resaltado de texto.

# Edición básica: Resaltar texto, listas, texto de código y separadores

Pero en este párrafo voy a resaltar la palabra **negrita** y la palabra *cursiva*. También puedo hacer ***cursiva y negrita a la vez***. También se puede ~tachar~, y con barra inclinada para atrás omites un caracter de markdown, por ejemplo \*\*esto no son negritas**.

Ahora voy a hacer una lista identificada con bullets:

* lista elemento 1

* lista elemento 2

Las listas anidadas se identifican añadiendo cuatro espacios antes:

* elemento 1
    * apartado 1.1
    * apartado 1.2

~~~
código
~~~

`código`

texto sencillo pero voy a meter un salto de linea ahora  
nueva linea.

Ahora separaro con una líneas
***

También vale

---

Es decir, tienen que ser al menos tres asteriscos, tres guiones o tres guiones bajos, pero pueden ser 4, 5, 6... lo importante que sean más de tres. También vale si inserto espacios por el medio, para hacerlo más visible en el texto plano:
* * *

Pero si uso guiones debo insertar un salto de línea porque si no lo identifica como un subrayado.

Ahora voy a hacer una lista identificada con números

1. elemento uno

2. elemento dos

Pero no hay esquemas numerados:

1. Elemento uno
    1. Elemento 1.1.
    2. Elemento 1.2.

Esto es un code block, como los correos electrónicos.

> code block

> 1. uno

Como se ve, el signo mayor \> añade una tabulación al comienzo. Pero ojo, algunos intérpretes de markdown usan el \> para las citas.

# Tablas

Las tablas se forman usando el símbolo de pipe para separar las columnas y los guiones (mínimo tres guiones para que se entienda como tabla) para separar el encabezado de la columna:

| columna 1 | columna 2 |
| --- | --- |
| otra cosa | dos cosas |
| cuatro cosas | cinco cosas |

Lo que le dice a un documento markdown "esto es una tabla" es la presencia de un encabezado, no la presencia de un símbolo de pipe. Por lo tanto, los tres guiones son imprescindibles. Y tiene que haber por lo menos tres guiones, no vale con dos.

En los guiones también se indica la alineación de los datos de la tabla, usando "dos puntos":

|columna 1|columna 2|columna 3|
|:---|---:|:---:|
|dato alineado a la izquierda| dato alineado a la derecha| dato centrado|
|y así|sucesivamente|con el resto|

# Otro tipo de encabezados

Este tipo de encabezado sirve sólo para dos niveles; el principal es añadiendo un subrayado doble  
usando el signo "="

Y el encabezado secundario lo mismo pero subrayando sencillo  
usando el guión "-"

Título 1
========

Título 2
--------

Y no hace falta subrayarlo todo, basta con un "=" o un "-". Por ejemplo, escribir  
Título 1  
=

Genera:

Título 1
=

Y escribir  
Título 2  
-

Genera:

Título 2
-

# Enlaces y referencias cruzadas

Se escribe el nombre del enlace entre corchetes \[nombre del enlace]  
y, seguido, en la misma línea, la url entre paréntesis \(url), es decir:  
\[nombre del enlace]\(url)

[este es el texto del enlace a internete](https://wikipedia.es)

que también se puede poner como texto la url, nadie te quita...

[https://wikipedia.es](https://wikipedia.es)

Pero en markdown se usan los símbolos de mayor y menor para conseguir el mismo efecto:

<https://www.wikipedia.es>

Y se puede hacer una especie de "alias", para no tener que usar la url cada vez que la quieras mencionar como un enlace. Los alias se usan igual que la url pero entre corchetes, es decir:  
\[nombre del enlace]\[alias]

Pero luego tienes que indicar la url que corresponde con ese alias:  
\[alias]:url

En este caso, la url no se mete entre paréntesis, se indica con "dos puntos".

A partir de tener un alias definido, puedo usarlo con distintos nombres del enlace, por ejemplo:

>`...mi página web versa sobre \[historia]\[mi_web]. En mi \[web]\[mi_web] encontrarás anécdotas, cosas curiosas...`  
>`\[mi_web]:http://www.yo.es`

...mi página web versa sobre [historia][mi_web]. En mi [web][mi_web] encontrarás anécdotas, cosas curiosas...

[mi_web]:http://www.yo.es

Fíjate como en un caso uso en nombre "historia" y en el siguiente "web", pero ambos nombres hacen referencia a la misma url, por el alias *mi_web*

Para insertar imágenes, se usa la misma sintaxis que para los enlaces, con tres detalles:

1. Se pone un signo de admiración al principio, para indicar que se trata de una imagen "!"
2. En vez de la url se indica la ruta a la imagen
3. En vez del nombre del enlace se indica un texto que será el que se muestre si la carga de la imagen falla

\!\[texto si la imagen no carga]\(/ruta/a/la/imagen)

## Resumen de enlaces

En general, un enlace en markdown se compone de un texto entre corchetes (el nombre que tendrá el enlace en el documento markdown) y la referencia al enlace entre paréntesis. Ahora, un resumen de los tipos de referencias que se pueden meter en esos paréntesis:

- Referencia a una url de internet: (url)
- Referencia a un archivo: (/file)
- Referencia a un encabezado del documento markdown: (#encabezado)
- Otra referencia a un encabezado del documento markdown: (URL "título")
- Referencia a un correo electrónico: (mailto:juan.silva@disroot.org)


# Créditos y Licencia

Este manual autointerpretable de Markdown lo ha realizado [Juan Antonio Silva Villar](mailto:juan.silva@disroot.org).

Puedes usar estos apuntes libremente, bajo las condiciones de la licencia Creative Commons Reconocimiento-CompartirIgual 4.0  
Detalles de la licencia en <http://creativecommons.org/licenses/by-sa/4.0/>

