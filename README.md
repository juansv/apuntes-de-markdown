# Descripción del contenido de este repositorio

## Tutorial de Markdown usando Markdown

El lenguaje de marcado [Markdown](https://es.wikipedia.org/wiki/Markdown) es muy sencillo de usar. El archivo `tutorial_markdown_usando_markdown.md` contiene ejemplos de uso del lenguaje Markdown... escritos en Markdown.

Puede leerse tanto en un editor de texto plano como en un visualizador de Markdown, y confío que en ambos casos se pueda interpretar sin problemas.

El objetivo último del archivo es servir de guía de referencia rápida para el uso de Markdown.

## Tutorial de Markdown para crear páginas del man

Gracias a programas como `pandoc` es muy sencillo convertir un archivo en formato Markdown en otros formatos, como pdf, html... o groff. Y groff es el lenguaje de marcado que se usa en la elaboración de las páginas del `man` en los SO GNU/linux.

Por lo tanto, puede usarse Markdown para escribir las páginas del man. La ventaja de usar Markdown en lugar de groff es que éste último es un lenguaje de marcado bastante más difícil de usar. Y mucho menos amigable a la hora de visualizar conforme lo editas.

En definitiva, el uso de Markdown para crear páginas del `man` simplifica mucho el trabajo, aunque haya que hacer un paso más, que es la conversión a groff.

En este tutorial se explica cómo se debe estructurar una página del `man` usando Markdown y la conversión a groff usando `pandoc`.

Para realizar este tutorial, me basé en la información disponible en <https://www.howtogeek.com/682871/how-to-create-a-man-page-on-linux/>. Este documento en línea fue realizado por Dave McKay, a quien se cita en los créditos de mi archivo. Por lo tanto, mi tutorial no es más que una interpretación traducida al castellano del documento del señor McKay.

## Plantilla Markdown para man

Este documento contiene una plantilla con la estructura básica de una página del `man` escrita en Markdown. La idea de la plantilla es que, para hacerte una entrada del `man`, hagas una copia de la plantilla, la modifiques con la información de tu programa y generes el `archivo.1` con `pandoc`. Así de simple.

