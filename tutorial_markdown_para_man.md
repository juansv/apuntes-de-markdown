USAR MARKDOWN PARA ESCRIBIR ENTRADAS DEL MAN
============================================

Las entradas del man están formateadas en un lenguaje de marcado llamado **groff**. Escribir en groff es más difícil que markdown. Pero existe un conversor de formatos muy potente llamado **pandoc**. Este conversor es la bomba, no sólo convierte entre markdown y groff, admite un montón de combinaciones de conversión de formatos (¡casi 40 formatos diferentes!). Incluso es capaz de usar como archivo de entrada (o convertir a) el formato de guord (.docx). Te invito a ver el `man` de pandoc para flipar un poco; luego me preguntan por qué uso GNU/linux...

Escribir tus entradas de man en markdown no sólo simplifica el proceso de creación, también el de edición futura.

A continuación, se muestra un ejemplo de un archivo formateado en markdown con la estructura de una entrada del man, de tal forma que la conversión markdown -> groff sea directamente un documento.1 típico de las entradas del man de GNU/linux.

En el ejemplo se indican la disposición mínima que debería tener una entrada del man, según la convención al respecto. Pero una entrada de man puede tener muchos más apartados. Un buen punto donde informarte sobre el contenido de una entrada del man es  
<https://www.freebsd.org/cgi/man.cgi?query=mdoc&sektion=7>  
En este mismo enlace hay algunas muestras del marcado de un documento groff, y se puede ver que es mucho más complicado que markdown.

Una entrada del man debe comenzar con el *front matter*, que son tres líneas que empiezan con "%" seguidas de un espacio y...

- La primera línea debe llevar el nombre del comando seguido, sin separación, por la [sección del manual](https://en.wikipedia.org/wiki/Man_page#Manual_sections) entre paréntesis. Esto aparecerá en la parte de arriba al mostrar el man, como un encabezado. Es una convención de las entradas del man. También por convención, el nombre del comando se escribe en mayúsculas en el encabezado. Lo que se escribe a continuación de la sección del manual, aparecerá en la parte de abajo, como un pie de página. En este punto se recomienda poner la versión del programa.
- La segunda línea es para el nombre del autor. Al hacer esto, se genera en automático una sección del man para el autor.
- La tercera línea es para la fecha, que se colocará en el centro del pie de página.

El ejemplo está auto-documentado, se interpreta leyéndolo. Se basa en un programa fictício, llamado `comando`.

EL EJEMPLO
==========

* * *

% COMANDO(1) comando 1.0.3
% NombreAutor Apellido
% Mayo 2022

# NAME
comando - comando es un comando que hace cosas de comandos.

# SYNOPSIS
**comando** [*OPCION*]\
**comando** *PATRON*

# DESCRIPTION
**comando** es un comando de la línea de comandos que hace cosas de comandos. Por ejemplo, saca un montón de texto por la consola, de tal forma que pareces un hacker del copón. Ejecutar **comando** te convierte en informático sin pasar por la universidad.
Por defecto, si al escribir la descripción pasas a otra línea con la tecla *intro*, la conversión al man insertará una línea en blanco de separación. Si no quieres que meta esa linea en blanco, debes cerrar la línea con un backslash (la barra para atrás: \).\
Esta línea no tiene separación.

## Caso particular
Cuando uses **comando**, mete alguna *pipe*, que parece más "pro" todavía.
Se pueden añadir subapartados en la descripción, pero sin pasarse: una entrada del man es una guía rápida de uso, no un manual de usuario a todo detalle.

# OPTIONS
**-h**, **--help**
: Muestra un mensaje de ayuda.

**-v**, **--verbose**
: Saca información por pantalla a cascoporro.

**-e**, **--entrada DATO**
: Introduce un DATO de entrada a comando, de tal forma que comando hace más cosas molonas.

# EXAMPLES
**comando**
: Muestra un mensaje con instrucciones de uso, la versión y hace exit.

**comando -e DATO**
: Devuelve el DATO multiplicado por *pi*, luego por **e**, le suma 3, le resta 5 y calcula el mínimo común múltiplo. ¿Y para qué? Pues para que salgan muchas cosas por pantalla!!!

**comando -e | comando -v**
: Hace lo anterior, pero sacando todavía más texto por pantalla, para que mole más.

# EXIT VALUES
**0**
: Si todo sale bien, devuelve 0

**1**
: El DATO no es correcto

# BUGS
Espero que no haya...

# AUTHOR
Al escribir en la parte de arriba, donde los "%", el nombre del autor, no hace falta cumplimentar esta sección. Se indica aquí sólo como recordatorio de que, la convención de las entradas del man, el AUTHOR es una se las secciones obligadas.

# COPYRIGHT

Copyright 2020 Juan Silva. Licencia GPLv3+: GNU GPL version 3 o posterior <https://gnu.org/licences/gpl.html>. Esto es software libre: puedes cambiarlo y distribuirlo libremente. No hay garantía, en la medida que lo permita la ley.

* * *

PANDOC AL RESCATE
=================

Ahora vemos el programa `pandoc` y los argumentos necesarios para convertir este markdown en una entrada del man. Para convertir de markdown a groff, usando *pandoc*, debemos ejecutar:

`pandoc comando.1.md -s -t man -o comando.1`

**-s** es el argumento *standalone*, es decir, genera una única página, con toda la información de arriba a abajo del fichero de entrada.

**-t** indica el tipo. En este caso, indicamos directamente que queremos un documento del `man` de linux.

**-o** indica el archivo de salida. Podríamos omitir este argumento y la información saldría por la salida estándar, de tal forma que la podríamos capturar con una pipe y llevarla donde nos interese.

CRÉDITOS
========

Este manual es una simple interpretación de <https://www.howtogeek.com/682871/how-to-create-a-man-page-on-linux/>. Sin documentación como la de este enlace, el mundo sería un lugar peor. Thank you Mr. Dave McKay!

[Juan Antonio Silva Villar](mailto:juan.silva@disroot.org)

LICENCIA
========

Puedes usar estos apuntes libremente, bajo las condiciones de la licencia Creative Commons Reconocimiento-CompartirIgual 4.0  
Detalles de la licencia en <http://creativecommons.org/licenses/by-sa/4.0/>

